-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2016 at 09:10 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sintec`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
`id_artikel` int(11) NOT NULL,
  `judul_artikel` varchar(250) NOT NULL,
  `seo_artikel` varchar(250) NOT NULL,
  `isi_artikel` text NOT NULL,
  `tanggal_posting` date NOT NULL,
  `viewed` int(11) NOT NULL,
  `kategori_artikel` int(11) NOT NULL,
  `gambar_artikel` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=303 ;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul_artikel`, `seo_artikel`, `isi_artikel`, `tanggal_posting`, `viewed`, `kategori_artikel`, `gambar_artikel`) VALUES
(301, 'Belajar Ngoding', 'belajar-ngoding', 'Test', '2016-02-16', 100, 401, '01_preview2.jpg'),
(302, 'Belajar COC', 'belajar-coc', 'Akeh', '2016-02-16', 0, 401, '01_preview2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `icon`
--

CREATE TABLE IF NOT EXISTS `icon` (
`id_icon` int(11) NOT NULL,
  `nama_icon` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=215 ;

--
-- Dumping data for table `icon`
--

INSERT INTO `icon` (`id_icon`, `nama_icon`) VALUES
(201, 'icon-user'),
(202, 'icon-user-female'),
(203, 'icon-users'),
(204, 'icon-user-follow'),
(205, 'icon-user-following'),
(206, 'icon-trophy'),
(207, 'icon-speedometer'),
(208, 'icon-social-youtube'),
(209, 'icon-social-twitter'),
(210, 'icon-social-tumblr'),
(211, 'icon-social-facebook'),
(212, 'icon-screen-tablet'),
(213, 'icon-screen-smartphone'),
(214, 'icon-screen-desktop');

-- --------------------------------------------------------

--
-- Table structure for table `kategoriartikel`
--

CREATE TABLE IF NOT EXISTS `kategoriartikel` (
`id_kategoriartikel` int(11) NOT NULL,
  `nama_kategoriartikel` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=402 ;

--
-- Dumping data for table `kategoriartikel`
--

INSERT INTO `kategoriartikel` (`id_kategoriartikel`, `nama_kategoriartikel`) VALUES
(401, 'Game');

-- --------------------------------------------------------

--
-- Table structure for table `kepengurusan`
--

CREATE TABLE IF NOT EXISTS `kepengurusan` (
`id_kepengurusan` int(11) NOT NULL,
  `tahun_kepengurusan` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kepengurusan`
--

INSERT INTO `kepengurusan` (`id_kepengurusan`, `tahun_kepengurusan`) VALUES
(1, '2015 / 2016');

-- --------------------------------------------------------

--
-- Table structure for table `menuadmin`
--

CREATE TABLE IF NOT EXISTS `menuadmin` (
`id_menuadmin` int(11) NOT NULL,
  `nama_menuadmin` varchar(100) NOT NULL,
  `link_menuadmin` varchar(100) NOT NULL,
  `is_aktif` tinyint(1) NOT NULL,
  `urutan_ke` int(11) NOT NULL,
  `icon_menuadmin` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `menuadmin`
--

INSERT INTO `menuadmin` (`id_menuadmin`, `nama_menuadmin`, `link_menuadmin`, `is_aktif`, `urutan_ke`, `icon_menuadmin`) VALUES
(101, 'Home', '', 1, 1, 'icon-user'),
(102, 'Artikel', 'artikel', 1, 2, 'icon-user'),
(103, 'Menu Admin', 'menuadmin', 1, 3, 'icon-screen-tablet');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE IF NOT EXISTS `pengurus` (
`id_pengurus` int(11) NOT NULL,
  `nama_pengurus` varchar(255) NOT NULL,
  `tahun_kepengurusan` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `kelas_pengurus` varchar(20) NOT NULL,
  `jabatan_pengurus` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`id_pengurus`, `nama_pengurus`, `tahun_kepengurusan`, `foto`, `kelas_pengurus`, `jabatan_pengurus`) VALUES
(1, 'Petra', 1, 'Jail-512.png', '11 MIPA 2', 'Ketua');

-- --------------------------------------------------------

--
-- Table structure for table `submenuadmin`
--

CREATE TABLE IF NOT EXISTS `submenuadmin` (
`id_submenuadmin` int(11) NOT NULL,
  `id_menuadmin` int(11) NOT NULL,
  `nama_submenuadmin` varchar(100) NOT NULL,
  `is_aktif` tinyint(1) NOT NULL,
  `urutan_ke` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tutorial`
--

CREATE TABLE IF NOT EXISTS `tutorial` (
`id_tutorial` int(11) NOT NULL,
  `judul_tutorial` varchar(255) NOT NULL,
  `seo_tutorial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
 ADD PRIMARY KEY (`id_artikel`), ADD KEY `kategori_artikel` (`kategori_artikel`), ADD KEY `kategori_artikel_2` (`kategori_artikel`);

--
-- Indexes for table `icon`
--
ALTER TABLE `icon`
 ADD PRIMARY KEY (`id_icon`);

--
-- Indexes for table `kategoriartikel`
--
ALTER TABLE `kategoriartikel`
 ADD PRIMARY KEY (`id_kategoriartikel`);

--
-- Indexes for table `kepengurusan`
--
ALTER TABLE `kepengurusan`
 ADD PRIMARY KEY (`id_kepengurusan`);

--
-- Indexes for table `menuadmin`
--
ALTER TABLE `menuadmin`
 ADD PRIMARY KEY (`id_menuadmin`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
 ADD PRIMARY KEY (`id_pengurus`);

--
-- Indexes for table `submenuadmin`
--
ALTER TABLE `submenuadmin`
 ADD PRIMARY KEY (`id_submenuadmin`), ADD KEY `id_menuadmin` (`id_menuadmin`);

--
-- Indexes for table `tutorial`
--
ALTER TABLE `tutorial`
 ADD PRIMARY KEY (`id_tutorial`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT for table `icon`
--
ALTER TABLE `icon`
MODIFY `id_icon` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=215;
--
-- AUTO_INCREMENT for table `kategoriartikel`
--
ALTER TABLE `kategoriartikel`
MODIFY `id_kategoriartikel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=402;
--
-- AUTO_INCREMENT for table `kepengurusan`
--
ALTER TABLE `kepengurusan`
MODIFY `id_kepengurusan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menuadmin`
--
ALTER TABLE `menuadmin`
MODIFY `id_menuadmin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
MODIFY `id_pengurus` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `submenuadmin`
--
ALTER TABLE `submenuadmin`
MODIFY `id_submenuadmin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tutorial`
--
ALTER TABLE `tutorial`
MODIFY `id_tutorial` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
