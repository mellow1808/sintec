<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	var $data;
	function __construct(){
		parent::__construct();
		$this->load->library('template_admin');
		$this->load->model('admin_model');
		$this->load->model('menuadmin/menuadmin_model');
		$this->load->model('icon/icon_model');
		$this->load->model('artikel/artikel_model');
		$this->load->model('kategoriartikel/kategoriartikel_model');

		$this->data['sidebar'] = $this->admin_model->getSidebar();
		$this->data['breadcrumb'] = $this->admin_model->getBreadcrumb();
	}
	
	public function index(){
		$data = $this->data;
		$this->template_admin->display('admin/home', $data);
	}
	public function artikel($aksi = 'index', $id=null){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
				$data['artikel'] = $this->artikel_model->getArtikel();
				$this->template_admin->display('admin/content/artikel/index', $data);
				break;
			case 'add':
				$data['kategori_artikel'] = $this->kategoriartikel_model->getKategoriartikel();
				$this->template_admin->display('admin/content/artikel/add', $data);
				break;
			case 'edit':
				$this->template_admin->display('admin/content/artikel/edit', $data);
				break;
			case 'insert':
				$this->artikel_model->insert();
				break;
			case 'update':
				# code...
				break;
			case 'delete':
				# code...
				break;
			default:
				# code...
				break;
		}
	}
	public function menuadmin($aksi = 'index', $id=null){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
				$data['menuadmin'] = $this->menuadmin_model->getMenuadmin();
				$this->template_admin->display('admin/content/menuadmin/index', $data);
				break;
			case 'add':
				$data['icon'] = $this->icon_model->getIcon();
				$data['available_index'] = $this->menuadmin_model->getAvailableIndex();
				$this->template_admin->display('admin/content/menuadmin/add', $data);
				break;
			case 'edit':
				$data['icon'] = $this->icon_model->getIcon();
				$data['available_index'] = $this->menuadmin_model->getAvailableIndex();
				$data['menuadmin'] = $this->menuadmin_model->getMenuadminById($id);
				$this->template_admin->display('admin/content/menuadmin/edit', $data);
				break;
			case 'insert':
				$this->menuadmin_model->insert();
				redirect('admin/menuadmin');
				break;
			case 'update':
				$this->menuadmin_model->update($id);
				redirect('admin/menuadmin');
				break;
			case 'delete':
				# code...
				break;
			default:
				# code...
				break;
		}
	}
}
