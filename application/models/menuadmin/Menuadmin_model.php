<?php
	class Menuadmin_model extends CI_Model{
		public function getMenuadmin(){
			$menuadmin = $this->db->query("SELECT * FROM menuadmin ORDER BY id_menuadmin ASC");
			$menuadmin = $menuadmin->result();
			return $menuadmin;
		}
		public function getMenuadminByid($id){
			$menuadmin = $this->db->query("SELECT * FROM menuadmin WHERE id_menuadmin = '$id'");
			return $menuadmin->row();
		}
		public function getAvailableIndex(){
			$used = $this->db->query("SELECT DISTINCT urutan_ke FROM menuadmin");
			$available = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
			$used_value = [];
			$index = 0;
			foreach ($used->result() as $q_used) {
				$used_value[$index] = $q_used->urutan_ke;
			$index++;
			}
			$available = array_diff($available, $used_value);
			return $available;
		}
		public function insert(){
			$nama = $this->input->post('nama');
			$link = $this->input->post('link');
			$aktif = $this->input->post('aktif');
			$icon = $this->input->post('icon');
			$urutan = $this->input->post('urutan');

			$data = [
				'nama_menuadmin' => $nama,
				'link_menuadmin' => $link,
				'is_aktif' => $aktif,
				'icon_menuadmin' => $icon,
				'urutan_ke' => $urutan
			];

			$this->db->insert('menuadmin', $data);
		}
		public function update($id){
			$nama = $this->input->post('nama');
			$link = $this->input->post('link');
			$aktif = $this->input->post('aktif');
			$icon = $this->input->post('icon');
			$urutan = $this->input->post('urutan');

			$data = [
				'nama_menuadmin' => $nama,
				'link_menuadmin' => $link,
				'is_aktif' => $aktif,
				'icon_menuadmin' => $icon,
				'urutan_ke' => $urutan
			];
			$where = [
				'id_menuadmin' => $id
			];

			$this->db->update('menuadmin', $data, $where);
		}

	}