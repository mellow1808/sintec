				<!-- footer 
					================================================== -->
				<footer>
					<div class="up-footer">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<div class="footer-widget">
										<h2>About Us</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipi sicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna.</p>
										<img src="images/footer-logo.png" alt="">
									</div>
								</div>
								<div class="col-md-4">
									<div class="footer-widget">
										<h2>Flickr widget</h2>
										<ul class="flickr">
											<li><a href="#"><img src="upload/flickr/1.jpg" alt=""></a></li>
											<li><a href="#"><img src="upload/flickr/2.jpg" alt=""></a></li>
											<li><a href="#"><img src="upload/flickr/3.jpg" alt=""></a></li>
											<li><a href="#"><img src="upload/flickr/4.jpg" alt=""></a></li>
											<li><a href="#"><img src="upload/flickr/5.jpg" alt=""></a></li>
											<li><a href="#"><img src="upload/flickr/6.jpg" alt=""></a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-4">
									<div class="footer-widget info-widget">
										<h2>Working Hours</h2>
										<p class="first-par">You can contact or visit us during working time.</p>
										<p><span>Tel: </span> 1234 - 5678 - 9012</p>
										<p><span>Email: </span> nunforest@gmail.com</p>
										<p><span>Working Hours: </span> 8:00 a.m - 17:00 a.m</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<p class="copyright">
						&copy; Copyright 2014. "Koncept" by Nunforest. All rights reserved.
					</p>
				</footer>
				<!-- End footer -->